from src.nodos import Nodo


# Definicion de la clase 'ListaLigada':
class ListaLigada:
    def __init__(self):
        self.cabeza = None

    # Agrega un elemento al final de la lista
    def agregar_elemento(self, valor):
       nuevo_nodo = Nodo(valor)
       if self.cabeza is None: # Si la lista está vacía, el nuevo nodo será la cabeza
           self.cabeza = nuevo_nodo 
       else: # Si no, recorremos la lista hasta encontrar el último nodo
           actual = self.cabeza
           while actual is not None:
               actual = actual.siguiente
        # Al salir del ciclo, actual apunta al último nodo
               actual.siguiente = nuevo_nodo   # Hacemos que el último nodo apunte al nuevo nodo
  
    def buscar_elemento(self, valor):
        actual = self.cabeza # Empezamos desde la cabeza
        while actual is not None: # Mientras no lleguemos al final de la lista
            if actual.valor == valor: # Si encontramos el valor buscado
                return True # Regresamos True
            actual = actual.siguiente # Pasamos al siguiente nodo
    # Si salimos del ciclo, significa que no encontramos el valor
        return F
    # Regresamos False

    def elimina_cabeza(self):
        if self.cabeza is not None: # Si la lista no está vacía
            self.cabeza = self.cabeza.siguiente # Hacemos que la cabeza sea el segundo nodo

    def elimina_rabo(self):
        if self.cabeza is not None:
            if self.cabeza.siguiente is None:
                self.cabeza = None 
            else: # Si hay más de un nodo en la lista
                actual = self.cabeza
                anterior = None
                while actual.siguiente is not None:# Recorremos la lista hasta encontrar el último nodo
                    anterior = actual # Guardamos el nodo anterior al actual
                    actual = actual.siguiente
                # Al salir del ciclo, actual apunta al último nodo y anterior al penúltimo    
                actual.siguiente = None # Hacemos que el penúltimo nodo apunte a None

    def tamagno(self):
        # Aqui inicia tu codigo
        contador = 0 # Inicializamos un contador en cero
        actual = self.cabeza # Empezamos desde la cabeza
        while actual is not None: # Mientras no lleguemos al final de la lista
            contador += 1 # Incrementamos el contador en uno
            actual = actual.siguiente # Pasamos al siguiente nodo
        return contador
        

    def copia(self):
        nueva_lista = ListaLigada()
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)
            actual = actual.siguiente
        return nueva_lista

    def __str__(self):
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)
  
